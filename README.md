# PairWise TI and Alpha Coherence Computation  
This program perform pairwise operation to calculate estimated TI for each section of each pair of subject, follow the algorithm described in the publication of Dikker et al.  

## Input
The program will ask user to choose a folder.  

Intput: 1 folder with this structure.
  
`data_folder`  
|  
|&ndash;&ndash;&ndash;&ndash; `AB01_subject_folder`  
|&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |  
|&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |&ndash;&ndash;&ndash;&ndash; `Chief1Section1_song`  
|&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch1.eph`  
|&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch2.eph`   
|&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `flag.csv`  
|&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |  
|&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |&ndash;&ndash;&ndash;&ndash; `Chief1Section2_pause`  
|&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch1.eph`  
|&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch2.eph`   
|&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `flag.csv`  
|&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |  
|&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |&ndash;&ndash;&ndash;&ndash; `Chief1Section3_music`  
|&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch1.eph`  
|&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch2.eph`   
|&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `flag.csv`  
|  
|&ndash;&ndash;&ndash;&ndash; `CD02_subject_folder`  
&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |  
&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |&ndash;&ndash;&ndash;&ndash; `Chief1Section1_song`  
&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch1.eph`  
&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch2.eph`   
&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `flag.csv`   
&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |  
&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |&ndash;&ndash;&ndash;&ndash; `Chief1Section2_pause`  
&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch1.eph`  
&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch2.eph`   
&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `flag.csv`  
&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |  
&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |&ndash;&ndash;&ndash;&ndash; `Chief1Section3_music`  
&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch1.eph`  
&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `epoch2.eph`  
&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  |&ndash;&ndash;&ndash;&ndash; `flag.csv`  

## Caution
- [-Please make sure that for each section, every subject has the same number of epoch-]  
- [-The number of epoch in each section folder is correspond to 'flag.csv'-]  

The 'flag.csv' describe the accepted and rejected epoch according to time.  
Ex: 1,0,1,1,0 means that the section has overall 5 seconds, and the accepted epoch is the epoch of second 1,3 and 4.  
  
You can change minimum epoch to consider in the file functions/TIPair.m  
line 15 variable threshold_epoch = ***;  

## Dependencies

- This program developed in [+MATLAB 2016b+] environments
- MATLAB Statistic Toolboxes
- MATLAB Signal Toolboxes