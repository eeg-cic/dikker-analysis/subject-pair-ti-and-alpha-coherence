clear;
clc;

% This program perform pairwise operation to calculate estimated TI for
% each section of each pair of subject, follow the algorithm described in
% the publication of Dikker et al.

% The required data structure can be found in the folder 'data_example'
% The 'flag.csv' describe the accepted and rejected epoch according to time.
% Ex: 1,0,1,1,0 means that the section has overall 5 seconds, and the
% accepted epoch is the epoch of second 1,3 and 4.

% Make sure that 
% - The number of epoch in each section folder is correspond to 'flag.csv'.
% - The number of sections is the same between each subject.

addpath('functions')
% Dependencies
% - This program developed in MATLAB 2016b environments
% - MATLAB Statistic Toolboxes
% - MATLAB Signal Toolboxes
% - Matlab functions
% --- CommonEpochCascade.m
% --- openeph.m

% Get the folder of all data
upper_folder = uigetdir; 

% List all subject folder
subject_folder = dir(upper_folder);
subject_folder = subject_folder([subject_folder(:).isdir]==1);
subject_folder = subject_folder(~ismember({subject_folder.name},{'.','..'}));

n_subject = length(subject_folder);
n_permutation = factorial(n_subject)/(2*factorial(n_subject-2));

firstcolumn_iscreate = 0;
header = {'section'};
header_all_elec_TI = {'section'};
header_all_elec_alpha = {'section'};
header_epoch = {'section'};
% For loop to get subject 1
for subject1 = 1:(length(subject_folder)-1)
    subject1_name = subject_folder(subject1).name;
    subject1_path = strcat(upper_folder,filesep,subject1_name);
    
    % List all section in subject 1
    section1_folder = dir(subject1_path);
    section1_folder = section1_folder([section1_folder(:).isdir]==1);
    section1_folder = section1_folder(~ismember({section1_folder.name},{'.','..'}));
    
    % Create the first column of output table
    if firstcolumn_iscreate == 0
        first_column = cell(length(section1_folder),1);
        for n_section = 1:length(section1_folder)
            first_column{n_section} = section1_folder(n_section).name;
        end
        final_epoch = first_column;
        first_column(end+1) = {'Avg'};
        final_output = first_column;
        final_output_all_elec_TI = first_column;
        final_output_all_elec_alpha = first_column;
        firstcolumn_iscreate = 1;
    end
    
    % For loop to get subject 2
    for subject2 = (subject1+1):length(subject_folder)
        subject2_name = subject_folder(subject2).name;
        subject2_path = strcat(upper_folder,filesep,subject2_name);
        
        % List all section in subject 2
        section2_folder = dir(subject2_path);
        section2_folder = section2_folder([section2_folder(:).isdir]==1);
        section2_folder = section2_folder(~ismember({section2_folder.name},{'.','..'}));
        
        % Check if the number of section between 2 subject is equal
        if length(section2_folder) == length(section1_folder)          
            % For loop to do pair wise operation between... 
            % Subject1,SectionX  and  Subject2,SectionX
            one_pair = cell(length(section1_folder),2);
            one_pair_all_elec_TI = cell(length(section1_folder),14);
            one_pair_all_elec_alpha = cell(length(section1_folder),14);
            number_of_epoch = cell(length(section1_folder),1);
            for pairwise = 1:length(section1_folder)
                section1_path = strcat(subject1_path,filesep,section1_folder(pairwise).name);
                section2_path = strcat(subject2_path,filesep,section2_folder(pairwise).name);
                [x,y] = CommonEpochCascade(section1_path,section2_path);
                number_of_epoch{pairwise, 1} = size(x,1)/128; 
                [TI,alphacoh,Ti_elec,alpha_elec] = TIPair(x,y);
                one_pair{pairwise,1} = TI;
                one_pair{pairwise,2} = alphacoh;   
                one_pair_all_elec_TI(pairwise,1:14) = num2cell(Ti_elec);
                one_pair_all_elec_alpha(pairwise,1:14) = num2cell(alpha_elec); 
            end
        end
        
        % For mean of all elctrode
        TI_row = cell2mat(one_pair(:,1));  
        mean_TI = sum(TI_row) / sum(TI_row~= 0);
        alphacoh_row = cell2mat(one_pair(:,2));  
        mean_alphacoh = sum(alphacoh_row) / sum(alphacoh_row~= 0);
        one_pair(end+1,:) = {mean_TI mean_alphacoh};
        
        final_output = [final_output one_pair];
        
        % Create header for final result table
        pair_name_TI = [subject1_name '-' subject2_name '-TI'];
        pair_name_alphaCoh = [subject1_name '-' subject2_name '-alphaCoh'];
        header = [header pair_name_TI pair_name_alphaCoh];
        
        % For each electrode TI
        if sum(sum(cell2mat(one_pair_all_elec_TI)~=0)) > 0
            mean_elec_TI = sum(cell2mat(one_pair_all_elec_TI),1)./sum(cell2mat(one_pair_all_elec_TI)~=0);
        else
            mean_elec_TI = zeros(1,14);
        end
        one_pair_all_elec_TI = [one_pair_all_elec_TI ; num2cell(mean_elec_TI)];
        final_output_all_elec_TI = [final_output_all_elec_TI one_pair_all_elec_TI];
        header_all_elec_TI = [header_all_elec_TI pair_name_TI cell(1,13)];
        
        % For each electrode alpha
        if sum(sum(cell2mat(one_pair_all_elec_alpha)~=0)) > 0
            mean_elec_alpha = sum(cell2mat(one_pair_all_elec_alpha),1)./sum(cell2mat(one_pair_all_elec_alpha)~=0);
        else
            mean_elec_alpha = zeros(1,14);
        end
        one_pair_all_elec_alpha = [one_pair_all_elec_alpha ; num2cell(mean_elec_alpha)];
        final_output_all_elec_alpha = [final_output_all_elec_alpha one_pair_all_elec_alpha];
        header_all_elec_alpha = [header_all_elec_alpha pair_name_alphaCoh cell(1,13)];
        
        % For epoch report
        final_epoch = [final_epoch number_of_epoch];
        header_epoch = [header_epoch [subject1_name '-' subject2_name]];

    end
    
end

final_output = [header ; final_output];
writetable(cell2table(final_output),'result.xlsx','FileType','spreadsheet','WriteVariableNames',false);

final_epoch = [header_epoch ; final_epoch];
writetable(cell2table(final_epoch),'epoch_reportt.xlsx','FileType','spreadsheet','WriteVariableNames',false);

electrode_header = {'electrode'};
electrode_list = {'AF3','F7','F3','FC5','T7','P7','O1','O2','P8','T8','FC6','F4','F8','AF4'};
for np = 1:n_permutation
    electrode_header = [electrode_header electrode_list];
end

final_output_all_elec_TI = [header_all_elec_TI ; electrode_header ; final_output_all_elec_TI];
writetable(cell2table(final_output_all_elec_TI),'result_each_electrode_TI.xlsx','FileType','spreadsheet','WriteVariableNames',false);
final_output_all_elec_alpha = [header_all_elec_alpha ; electrode_header ; final_output_all_elec_alpha];
writetable(cell2table(final_output_all_elec_alpha),'result_each_electrode_Alpha.xlsx','FileType','spreadsheet','WriteVariableNames',false);